﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoundaryManager : MonoBehaviour {

    private void OnTriggerExit(Collider other)
    {
        Debug.Log(other);
        Destroy(other.gameObject);
    }

}
