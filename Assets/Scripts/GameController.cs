﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class GameController : MonoBehaviour {

    public GameObject hazard;
    public Vector3 spawnValues;

    public int hazardCount;
    public float spawnWait;
    public float startWait;
    public float waveWait;

    private bool gameOver;

    public Text scoreText;
    private int score;

    public Text gameOverText;
    public Text restartText;
    

    private void Start()
    {
        InitGame();
        UpdateScore();
        StartCoroutine(SpawnWaves());
    }

    private void Update()
    {
        if (gameOver)
        {
            if (Input.GetKeyUp(KeyCode.R))
            {
                InitGame();
                SceneManager.LoadScene("Main");
            }
        }
    }

    private IEnumerator SpawnWaves()
    {
        yield return new WaitForSeconds(startWait);
        while (!gameOver)
        {
            for (int i = 0; i < hazardCount; i++)
            {
                Vector3 spawnPosition =
               new Vector3(Random.Range(-spawnValues.x, spawnValues.x),
               spawnValues.y,
               spawnValues.z);
                /* no rotation, but each one spins based on it's own rotation script. */
                Quaternion spawnRotation = Quaternion.identity;
                Instantiate<GameObject>(hazard, spawnPosition, spawnRotation);

                yield return new WaitForSeconds(spawnWait);
            }
            yield return new WaitForSeconds(waveWait);

            if (gameOver)
            {
                restartText.text = "PRESS R TO PLAY AGAIN.";
            }
        }
    }

    public void AddScore(int points)
    {
        score += points;
        UpdateScore();
    }

    public void GameOver()
    {
        gameOver = true;
        gameOverText.text = "GAME OVER";

    }
    
    private void InitGame()
    {
        score = 0;
        gameOver = false;
        gameOverText.text = "";
        restartText.text = "";
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }
    
    void UpdateScore()
    {
        scoreText.text = string.Format("Score: {0}", score.ToString());
    }
}
