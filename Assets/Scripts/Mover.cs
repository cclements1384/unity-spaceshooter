﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mover : MonoBehaviour {

    public float Speed;
   
	// Use this for initialization
	void Start () {
        GetComponent<Rigidbody>().velocity = transform.forward * Speed;
	}

}
