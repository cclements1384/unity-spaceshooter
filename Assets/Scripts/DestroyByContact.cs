﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyByContact : MonoBehaviour {

    public GameObject asteroidExplosion;
    public GameObject playerExplosion;
    private GameController gameController;
    public int scorevalue;

    private void Start()
    {
        gameController = GameObject
            .FindGameObjectWithTag("GameController")
            .GetComponent<GameController>();
    }


    private void OnTriggerEnter(Collider other)
    {
        /* gaurd against the boundary. */
        if (other.CompareTag("Boundary"))
            return;
        /* Destroy the objects. */
        Destroy(other.gameObject);
        Destroy(gameObject);

        /* Decide what explosion to play */
        if (other.CompareTag("Player"))
        {
            Explode(playerExplosion);
            gameController.GameOver();
        }
        else
        {
            Explode(asteroidExplosion);
            gameController.AddScore(scorevalue);
        }
          
    }

    private void Explode(GameObject explosion)
    {
        Instantiate<GameObject>(explosion,
               transform.position,
               transform.rotation);
    }
}
