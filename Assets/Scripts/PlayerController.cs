﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* We have to add this line to the properties will show in the inspector */
[System.Serializable]
public class Boundary
{
    public float X_MIN, X_MAX, Z_MIN, Z_MAX;
}


public class PlayerController : MonoBehaviour {

    /* player control */
    private Rigidbody rb;
    public float speed;
    public float tilt;
    /* player clamped to game area */
    public Boundary boundary;

    /* spawning the shot */
    public Transform shotSpawn;
    public GameObject shot;

    /* limiting the rate of fire */

    private float nextFire = 0.0f;
    public float rateOfFire = 0.5f;
        
    private void Update()
    {
        if (Input.GetKeyUp(KeyCode.Space) && Time.time > nextFire)
        {
            nextFire = Time.time + rateOfFire;

            Instantiate<GameObject>(
                shot, 
                shotSpawn.position,
                shotSpawn.rotation);

            GetComponent<AudioSource>().Play();
        }
    }

    private void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");
        Rigidbody rb = GetComponent<Rigidbody>();

        /* Add movements to the character */
        Vector3 movement =  new Vector3(moveHorizontal, 0.0f, moveVertical);
        rb.velocity = movement * speed;

        rb.position = new Vector3(
                Mathf.Clamp(rb.position.x,boundary.X_MIN , boundary.X_MAX), 
                0.0f,
                Mathf.Clamp(rb.position.z, boundary.Z_MIN, boundary.Z_MAX)
            );

        /* tilt the ship when moving back and forth. */
        rb.rotation = Quaternion.Euler(0.0f, 0.0f, rb.velocity.x *- tilt);
    }
}


